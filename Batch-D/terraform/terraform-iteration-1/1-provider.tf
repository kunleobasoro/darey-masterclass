provider "aws" {
  region = "us-east-1"
}

terraform {
  required_version = ">= 1.2.2"
  # backend "s3" {
  #   bucket = "devops-aker-academy"
  #   key    = "terraformstate/terraform.tfstate"
  #   region = "eu-west-2"
  # }
}